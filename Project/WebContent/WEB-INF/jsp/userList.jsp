<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
</head>

<body>

	<ul>
     <li class="navbar-text">${userInfo.name} さん </li>
     <li class="dropdown"><a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a></li>
    </ul>

</body>

<body>
<form class="form-signin" action="NewUser" method="post">
	<div align="center">
	<h1>ユーザ一覧</h1>
	</div>
	<div align="right">
    <a href="NewUser">新規登録</a>
    </div>
</form>





<form class="form-signin" action="UserListServlet" method="post">

	<Table Row>
	    <Tabele Header>ログインID　　　　　　　　　　　　　</Tabele Header>
		  <input type="text" name="loginId" value="" size="39">
	</Table Row>

	<Table Row>
	    <Tabele Header>ユーザー名　　　　　　　　　　　　　</Tabele Header>
		  <input type="text" name="user" value="" size="39">
	</Table Row>

	<Table Row>
		 <Tabele Header>生年月日　　　　　　　　　　　　　　</Tabele Header>
           <input type="date" name="date-start" value="" size="30">
            ~
           <input type="date" name="date-end" value="" size="30">

		  <input type="submit" name="submit" value="検索">
	</Table Row>
</div>
	<hr>

	<table border="1">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >

                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->


<!--●${userInfo.loginId=='admin'}➡お互い閉じてはいけない✖${userInfo.loginId}=={'admin'} -->
             <c:choose>
                  <c:when test="${userInfo.loginId=='admin'}">
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                     </td>
				 </c:when>
					<c:otherwise>
					<c:if test="${userInfo.loginId==user.loginId}">
					<td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>

                    </td>
                    </c:if>
					</c:otherwise>
			</c:choose>

                   </tr>
                 </c:forEach>
               </tbody>
             </table>
 </form> </body>
</html>
