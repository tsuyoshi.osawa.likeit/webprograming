<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報詳細参照</title>
</head>
<body>



	<table border="0">
	<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	</table>

</body>

<body>
						<div align="center">
						<h1>ユーザ情報詳細参照</h1>
 <form action="UserDatailServlet" method="post">
    <table>

	<tr>
	    <th>ログインID</th>
	    <td><c:out value="${user.loginId}"/></td>
	</tr>

	<tr>
	    <th>ユーザ名</th>
	    <td><c:out value="${user.name}"/></td>
	</tr>
	<tr>
	    <th>生年月日</th>
	    <td><c:out value="${user.birthDate}"/></td>
	</tr>
	<tr>
	    <th>登録日時</th>
	    <td><c:out value="${user.createDate}"/></td>
	</tr>
	<tr>
	    <th>更新日時</th>
	    <td><c:out value="${user.updateDate}"/></td>
	</tr>
    </table>

</form></div>

 <a href="UserListServlet" class="navbar-link login-link">戻る</a>
</body>
</html>
