<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザ情報更新</title>
</head>
<body>
	<table border="0">
		<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
	</table><hr>
</body>


<body>
	<form class="form-signin" action="UserUpdateServlet" method="post">
		<div align="center">
			<h1>ユーザ情報詳細</h1>
			<input type="hidden" name="id" value="${user.id}">
			<input type="hidden" name="loginId" value="${user.loginId}">
			<table>
				<tr>
					<th>ログインID</th>
					<td><c:out value="${user.loginId}" /></td>
				</tr>

				<tr>
					<th>パスワード</th>
					<td><input type="text" name="password"></td>
				</tr>

				<tr>
					<th>パスワード(確認)</th>
					<td><input type="text" name="passwordConfirmation"></td>
				</tr>

				<tr>
					<th>ユーザ名</th>
					<td><input type="text" name="userName" value="${user.name}"></td>

				</tr>
				<tr>
					<th>生年月日</th>
					<td><input type="text" name="userBirthDate" value="${user.birthDate}"></td>
				</tr>

			</table>
		</div>


		<div align="center">
			<br> <input type="submit" value="更新">

<p><font color="red">
			<c:if test="${errMsg != null}">
				<div class="alert alert-danger" role="alert">${errMsg}</div>
			</c:if>

			<c:if test="${errMsg1 != null}">
				<div class="alert alert-danger" role="alert">${errMsg1}</div>
			</c:if></font></p>
		</div>

		<br> <a href="UserListServlet" class="navbar-link login-link">戻る</a>
	</form>
</body>


</html>